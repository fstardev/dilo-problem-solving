﻿using System;
using UnityEngine;

namespace Controller
{

    public class Movement: MonoBehaviour
    {
        [SerializeField] private float speed = 10f;
        private Rigidbody2D m_rb2d;
        private const float BoundaryX = 8f;
        private const float BoundaryY = 4.4f;
        private Camera m_camera;

        private void Awake()
        {
            m_camera = Camera.main;
            m_rb2d = GetComponent<Rigidbody2D>();
        }

        private void Update()
        {
            var mousePosition= m_camera.ScreenToWorldPoint(Input.mousePosition);
            var direction = (mousePosition - transform.position).normalized;
            m_rb2d.velocity = new Vector2(direction.x * speed, direction.y * speed);
        }
        
    }
}