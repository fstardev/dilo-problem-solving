﻿using UnityEngine;

namespace Enemy
{
    public interface IFactory
    {
        GameObject SpawnEnemy(Vector3 position);
        int CountFactoryChildren();
    }
}