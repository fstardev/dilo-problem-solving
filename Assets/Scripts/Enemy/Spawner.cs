﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Enemy
{
    public class Spawner: MonoBehaviour
    {
        [SerializeField] private int minEnemy = 10;
        [SerializeField] private int maxEnemy = 14;
        [SerializeField] private EnemyFactory factory;

        private int m_totalEnemies = 5;
        private const int WrapY = 3, WrapX = 7;
        private void Start()
        {
            m_totalEnemies = Random.Range(minEnemy, maxEnemy);
            
            StartCoroutine(SpawnEnemyRoutine());
        }

        private IEnumerator SpawnEnemyRoutine()
        {
            var current = 1;
            while (current <= m_totalEnemies)
            {
                var spawnX = Random.Range(-WrapX, WrapX);
                var spawnY = Random.Range(-WrapY, WrapY);
                factory.SpawnEnemy(new Vector3(spawnX, spawnY, 0));
                current++;
            }

            yield return null;
        }
    }
}