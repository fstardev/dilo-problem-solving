﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Enemy
{
    public class EnemyFactory: MonoBehaviour, IFactory
    {
        [SerializeField] private GameObject prefab;
        [SerializeField] private GameObject prefabParent;
        public GameObject SpawnEnemy(Vector3 position)
        {
            return Instantiate(prefab, position, Quaternion.identity, prefabParent.transform);
        }


        public int CountFactoryChildren()
        {
            return 0;
        }
    }
}