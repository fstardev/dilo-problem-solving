﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Enemy
{
    public class Square: MonoBehaviour
    {

        private BoxCollider2D m_collider;
        [SerializeField] private float timer;
        [SerializeField] private bool isDead = false;


        private SpriteRenderer m_renderer;

        private void Awake()
        {
            m_collider = GetComponent<BoxCollider2D>();
            m_renderer = GetComponent<SpriteRenderer>();
        }

        private void Start()
        {
            var scaleX = Random.Range(0.5f, 1f);
            var scaleY = Random.Range(0.5f, 1f);
            gameObject.transform.localScale = new Vector3(scaleX, scaleY, 1);
            m_collider.size = new Vector2(scaleX, scaleY);
        }
        
        private void FixedUpdate()
        {
            if (m_renderer.enabled == false)
            {
                print("not active");
                timer += Time.fixedDeltaTime;
                if (timer > 3f)
                {
                    m_renderer.enabled = true;
                    timer = 0;
                }
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            print($"collide with: {other.gameObject.tag}");
            isDead = true;
            m_renderer.enabled = false;

        }

      
    }
}